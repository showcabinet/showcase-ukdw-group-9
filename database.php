<?php
require "connect-db.php";

function get_user($nim){
    $koneksi  = connect_db();
    $sql = "SELECT nim, first_name, last_name, email, password, secret_word, birthday, gender, prof_pict from users where nim = ?";
    $stmt = mysqli_prepare($koneksi, $sql);
    mysqli_stmt_bind_param($stmt, "s", $nim);
	mysqli_stmt_execute($stmt);
	mysqli_stmt_store_result($stmt);
    $user = array();
    if(mysqli_stmt_num_rows($stmt)) {
		mysqli_stmt_bind_result($stmt,$nim, $first_name, $last_name, $email, $password, $secret_word, $birthday, $gender, $prof_pict);
		mysqli_stmt_fetch($stmt);

		//ubah ke bentuk array
		$user = array("nim" => $nim, "first_name" => $first_name, "last_name" => $last_name, "email" => $email, "password" => $password, "secret_word" => $secret_word, "birthday" => $birthday, "gender" => $gender, "prof_pict" => $prof_pict);
	}
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
	return $user;
    
}

function get_quote($nim) {
	$koneksi = connect_db();
	$sql = "SELECT id_karya, judul_karya, tanggal_upload, gambar_karya, nim, keterangan FROM karya WHERE nim = ?";
	$stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "s", $nim);
	mysqli_stmt_execute($stmt);
	mysqli_stmt_bind_result($stmt, $id_karya, $judul_karya, $tanggal_upload, $gambar_karya, $nim, $keterangan);
    $kumpulan_karya = array();
	while(mysqli_stmt_fetch($stmt)) {
        $karya = array("id_karya" => $id_karya, "judul_karya" => $judul_karya, "tanggal_upload" => $tanggal_upload, "gambar_karya" => $gambar_karya, "nim" => $nim, "keterangan" => $keterangan);
		//ubah ke bentuk array
        
		$kumpulan_karya[] = $karya;
	}
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
	return $kumpulan_karya;
}

function add_quote($gambar, $nim, $judul, $keterangan) {
	$koneksi = connect_db();

	//escape input
	$nim = mysqli_real_escape_string($koneksi, $nim);
	$judul = mysqli_real_escape_string($koneksi, $judul);
	$keterangan = mysqli_real_escape_string($koneksi, $keterangan);
    
	$gambar = mysqli_real_escape_string($koneksi, $gambar);

	$sql = "INSERT INTO karya (judul_karya, gambar_karya, nim, keterangan)  VALUES (?,?,?,?)";
	$stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "ssss", $judul, $gambar, $nim, $keterangan);
	mysqli_stmt_execute($stmt);
	$newid = mysqli_insert_id($koneksi);
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
}

function change_photo($gambar, $nim) {
	$koneksi = connect_db();

	//escape input   
	$gambar = mysqli_real_escape_string($koneksi, $gambar);

	$sql = "UPDATE users SET prof_pict = ? WHERE nim = ?";
	$stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "ss", $gambar, $nim);
	mysqli_stmt_execute($stmt);
	$newid = mysqli_insert_id($koneksi);
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
    
    $user = get_user($nim);

       
        $_SESSION["NIM"] = $nim;
        $_SESSION["first_name"] = $user["first_name"];
        $_SESSION["last_name"] = $user["last_name"];
        $_SESSION["email"] = $user["email"];
        $_SESSION["gender"] = $user["gender"];
        $_SESSION["secret_word"] = $user["secret_word"];
        $_SESSION["birthday"] = $user["birthday"];
        $_SESSION["prof_pict"] = $user["prof_pict"];

        session_regenerate_id(true);
}



function get_recent_post() {
	$koneksi = connect_db();
	$sql = "SELECT id_karya, judul_karya, tanggal_upload, gambar_karya, nim, keterangan FROM karya";
	$stmt = mysqli_query($koneksi, $sql);
    
    $kumpulan_karya = array();
	while($row = mysqli_fetch_array($stmt, MYSQLI_ASSOC)) {
        $karya = array("id_karya" => $row["id_karya"], "judul_karya" => $row["judul_karya"], "tanggal_upload" => $row["tanggal_upload"], "gambar_karya" => $row["gambar_karya"], "nim" => $row["nim"], "keterangan" => $row["keterangan"]);
		//ubah ke bentuk array
        
		$kumpulan_karya[] = $karya;
	}
	mysqli_close($koneksi);
	return $kumpulan_karya;
}

function get_karya_by_id($id) {
	$koneksi = connect_db();
	$sql = "SELECT id_karya, judul_karya, tanggal_upload, gambar_karya, nim, keterangan FROM karya WHERE id_karya = ?";
	$stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "i", $id);
	mysqli_stmt_execute($stmt);
	mysqli_stmt_store_result($stmt);
    $karya = array();
	if(mysqli_stmt_num_rows($stmt)) {
		
	   mysqli_stmt_bind_result($stmt, $id_karya, $judul_karya, $tanggal_upload, $gambar_karya, $nim, $keterangan);
        mysqli_stmt_fetch($stmt);
        
        $karya = array("id_karya" => $id_karya, "judul_karya" => $judul_karya, "tanggal_upload" => $tanggal_upload, "gambar_karya" => $gambar_karya, "nim" => $nim, "keterangan" => $keterangan);
	}
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
	return $karya;
}

function get_user_data() {
	$koneksi  = connect_db();
    $sql = "SELECT * from users";
    $stmt = mysqli_query($koneksi, $sql);
    
    $users = array();
	while($row = mysqli_fetch_array($stmt, MYSQLI_ASSOC)) {
		//ubah ke bentuk array
        $user = array("nim" => $row["nim"], "first_name" => $row["first_name"], "last_name" => $row["last_name"], "email" => $row["email"], "password" => $row["password"], "secret_word" => $row["secret_word"], "birthday" => $row["birthday"], "gender" => $row["gender"], "prof_pict" => $row["prof_pict"]);
        
		$users[] = $user;
	}
	mysqli_close($koneksi);
	return $users;
}

 
function update_user($nim, $email) {
	$koneksi = connect_db();
	//escape input
	$sql = "UPDATE users SET email = ? WHERE nim = ?";
	$stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "ss", $email, $nim);
	mysqli_stmt_execute($stmt);
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
}


function delete_user($nim){
    $koneksi = connect_db();
	//escape input
	$sql = "DELETE from users WHERE nim = ?";
	$stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "s", $nim);
	mysqli_stmt_execute($stmt);
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
}

function update_user_self($nim, $email) {
	$koneksi = connect_db();
	//escape input
	$sql = "UPDATE users SET email = ? WHERE nim = ?";
	$stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "ss", $email, $nim);
	mysqli_stmt_execute($stmt);
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
    
        $user = get_user($nim);

       
        $_SESSION["NIM"] = $nim;
        $_SESSION["first_name"] = $user["first_name"];
        $_SESSION["last_name"] = $user["last_name"];
        $_SESSION["email"] = $user["email"];
        $_SESSION["gender"] = $user["gender"];
        $_SESSION["secret_word"] = $user["secret_word"];
        $_SESSION["birthday"] = $user["birthday"];
        $_SESSION["prof_pict"] = $user["prof_pict"];

        session_regenerate_id(true);
    }

function signin($nim, $email, $first_name, $last_name, $gender, $password, $secret_word, $birthday) {
    $koneksi = connect_db();
    $sql = "INSERT INTO users (nim,email,first_name, last_name, gender, password, secret_word,birthday) values ( ?, ?, ?, ?, ?, ?, ?, ?)";
    $stmt = mysqli_prepare($koneksi, $sql);
    mysqli_stmt_bind_param($stmt, "ssssssss", $nim, $email, $first_name, $last_name, $gender, $password, $secret_word,$birthday);
	mysqli_stmt_execute($stmt);
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
    
}

function delete_karya($id){
    $koneksi = connect_db();
	//escape input
	$sql = "DELETE from karya WHERE id_karya = ?";
	$stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "s", $id);
	mysqli_stmt_execute($stmt);
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
}
        


?>