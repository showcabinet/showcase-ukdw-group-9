<?php 
    session_start();

?> 

<!DOCTYPE html>
<html>
<head>
	<title>Showcase | Content</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="script/script.js"></script>
	
</head>
<body>
	
	<header id="back-header">
        <nav>
            <img src="img/menu-hamburger.svg" id="menu-hamburger" onclick="muncul('hidden-menu')" />
            <div onclick="window.location.href='index.php'">
                <img src="img/logo1.png" id="menu-home"  />
                <img src="img/logo2.png" class="logo2 animated-logo2"id="menu-home" onclick="window.location.href='index.php'"/>
            </div>
            
            <div id="hidden-menu" class="hide">
                <h3>Daftar Menu</h3>
                <ul style="list-style:none">
                    <li onclick="window.location.href='index.php'" class="pointer-cursor">Home Page</li>
                    <li onclick="window.location.href='profil.php'" class="pointer-cursor">Profile Page</li>
                    <hr>
                    <li onclick="window.location.href='semuaKarya.php'" class="pointer-cursor">Galery</li>
                    <li onclick="window.location.href='karyaFavorit.php'" class="pointer-cursor">Favorites</li>
                    <li onclick="window.location.href='karyaTerbaru.php'" class="pointer-cursor">New</li>
                    <hr>

                    <li><span onclick="window.location.href='aboutUs.php'" class="pointer-cursor">About Us</span></li>

                </ul>
            </div>
            
            
            <?php 
                if(isset($_SESSION["NIM"])){
                    echo '<div class="username-top"><a href="profil.php">';
                    echo $_SESSION["first_name"]; 
                    echo '</a> , <a href="logout.php">log out</a> </div>';
                }
                else
                {
                    echo
                    '
                    <div class="dropdown-click" id="login">
  			   <button onclick="myFunction()" class="login-btn" style="width: 90px" >Log in</button>
  			   <div id="Login-Click" class="dropdown-content">
                    <form action="login.php" method="post">
                        <div class="username-login">nim: <input type="text" name="nim"> </div>
    		            <div class="password-login">password: <input type="password" name="password"> </div>
    		            <div style="float:right; margin-bottom: 2px; margin-top:5px "> <button type="submit" value="submit">Log in</button> </div>
                        <div onclick="window.location.href="forgotPassword.php"" class="forgot-password">Forgot Password</div>
                    </form>
  			   </div>
		    </div>
            
            <div class="dropdown-click" id="signup">
  			   <button class="login-btn" style="width: 90px" onclick="window.location.href="signUp.php"">Sign Up</button>
  		    </div>
                    ';
                }
            ?>
            <form id="search" action="">
                <input type="search" name="search" placeholder="search keyword..." size="30"> 
                <button type="submit">Search</button>
            </form>
        </nav>
        
        
        <header id="bottom-header">
            <ul>
                <li class="nav-border pointer-cursor" onclick="window.location.href='index.php'"><span>Home</span></li>
                <li class="nav-border pointer-cursor" onclick="window.location.href='profil.php'"><span>Profile</span></li>
                <li class="nav-border pointer-cursor" onclick="window.location.href='semuaKarya.php'"><span>Galery</span></li>
            </ul>
                
        </header>
    </header>
	
	<div id="hidden-menu" class="hide">
		<h3>Daftar Menu</h3>
        <ul style="list-style:none">
            <li onclick="window.location.href='index.php'" class="pointer-cursor">Home Page</li>
            <li onclick="window.location.href='profil.php'" class="pointer-cursor">Profile Page</li>
            <hr>
            <li onclick="window.location.href='semuaKarya.php'" class="pointer-cursor">Galery</li>
            <li onclick="window.location.href='karyaFavorit.php'" class="pointer-cursor">Favorites</li>
            <li onclick="window.location.href='karyaTerbaru.php'" class="pointer-cursor">New</li>
            <hr>
            <li><span onclick="window.location.href='aboutUs.php'" class="pointer-cursor">About Us</span></li>
            
        </ul>
    </div>
	
	<div class="content">
        <div class="container">
                <center><img src="img/Nike-Jr-Mercurial-Vapor-X-Kids-Firm-Ground-Football-Boot-651620_803_A_PREM.jpg" style="height: 200px;"> </center>
            <center>Monday, February 22nd. By: Juan Christian</center>
            <div style="padding:10px">
                <h1>EXPLOSIVE SPEED</h1>
                <p>The Nike Jr. Mercurial Vapor X (4y-6y) Kids' Firm-Ground Soccer Cleat delivers high-speed control and lightweight explosiveness for the attacking forward. From the glove-like fit to the Vapor traction pattern, every element is designed for elite-level play at top speeds.</p>
                <h2>LOCKED-DOWN FIT</h2>
                <p>
                The tongue-less upper features ultra-thin Teijin microfiber synthetic leather that conforms to the shape of your foot for a second-skin fit.
                </p>
                <h2>ENHANCED TOUCH</h2>
                <p>The allover micro-textured upper helps grip the ball for excellent touch, and All Conditions Control (ACC) technology provides consistent control in wet or dry conditions.</p>
                <h2>EXPLOSIVE TRACTION</h2>
                <p>The Vapor traction pattern features direct-inject blades that dig and release quickly for exceptional traction in any direction. The forefoot grip zone, inspired by track spikes, provides toe-off traction for extra bursts of speed.</p>
                <h3>MORE BENEFITS</h3>
                <ul>
                    <li>Contoured, perforated sockliner for low-profile cushioning and reduced cleat pressure</li>
                    <li>Lightweight nylon chassis transfers energy directly to the cleats for enhanced responsiveness and stability</li>
                    <li>Firm-ground (FG) cleats for use on short-grass fields that may be slightly wet but rarely muddy</li>
                </ul>
                <br>
                <form>
                    Comment:<br>
                    <textarea name="comment" id="comment" style=" height:200px; width:400px ; "></textarea>
                    <br>
                    <input type='submit' value='Submit' />
                </form>
                <br>
                <img src="img/profile-img.jpg" style="width:100px; float: left"/>
                <p style="float:left; margin:0px; margin-left:10px"> <strong>Juan2212 <br>Thursday, 25 February. 23.00</strong></p>
                <br>
                <br>
                <p style="float: left; margin-left:10px"> Bagus yaa...</p>
            </div>
        </div>
		
        
	</div>
	
	<footer>
		<div> Dibuat oleh kelompok Show Cabinet UKDW 2014 </div>
	</footer>
	
	
</body>
</html>