<!DOCTYPE html>
<html>
<head>
	<title>Showcase | Buku</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="script/script.js"></script>
	
</head>
<body>
	
	<header id="back-header">
        <nav>
            <img src="img/menu-hamburger.svg" id="menu-hamburger" onclick="muncul('hidden-menu')" />
            <img src="img/logo1.png" id="menu-home" onclick="window.location.href='index.php'" />
            <img src="img/logo2.png" class="logo2 animated-logo2"id="menu-home">
            <div class="dropdown-click" id="login">
  			   <button onclick="myFunction()" class="login-btn" style="width: 90px" >Log in</button>
  			   <div id="Login-Click" class="dropdown-content">
                    <form>
                        <div class="username-login">username: <input type="text" name="username"> </div>
    		            <div class="password-login">password: <input type="password" name="password"> </div>
    		            <div style="float:right; margin-bottom: 2px; margin-top:5px "> <button type="submit" value="login">Log in</button> </div>
                        <div onclick="window.location.href='forgotPassword.php'" class="forgot-password">Forgot Password</div>
                    </form>
  			   </div>
		    </div>
            
            <div class="dropdown-click" id="signup">
  			   <button class="login-btn" style="width: 90px" onclick="window.location.href='signUp.php'">Sign Up</button>
  		    </div>
        </nav>
        
        
        <header id="bottom-header">
            <ul>
                <li class="nav-border pointer-cursor" onclick="window.location.href='index.php'"><span>Home</span></li>
                <li class="nav-border pointer-cursor" onclick="window.location.href='profil.php'"><span>Profile</span></li>
                <li class="nav-border pointer-cursor" onclick="window.location.href='semuaKarya.php'"><span>All Product</span></li>
            </ul>
                
        </header>
    </header>
	
	<div id="hidden-menu" class="hide">
		<h3>Daftar Menu</h3>
        <ul style="list-style:none">
            <li onclick="window.location.href='index.php'" class="pointer-cursor">Home Page</li>
            <li onclick="window.location.href='profil.php'" class="pointer-cursor">Profile Page</li>
            <hr>
            <li onclick="window.location.href='semuaKarya.php'" class="pointer-cursor">Semua Karya</li>
            <li onclick="window.location.href='karyaFavorit.php'" class="pointer-cursor">Karya Favorit</li>
            <li onclick="window.location.href='karyaTerbaru.php'" class="pointer-cursor">Karya Terbaru</li>
            <hr>
            <li onclick="munculList('hidden-list1')"><span class="pointer-cursor">Berdasarkan Kategori</span>
                <ul class="hide" id="hidden-list1">
                    <li onclick="window.location.href='journalPaper.php'" class="pointer-cursor">Paper dan Journal</li>
                    <li onclick="window.location.href='software.php'" class="pointer-cursor">Software</li>
                    <li onclick="window.location.href='game.php'" class="pointer-cursor">Game</li>
                    <li onclick="window.location.href='karyaIlmiah.php'"class="pointer-cursor">Karya Ilmiah</li>
                    <li onclick="window.location.href='buku.php'" class="pointer-cursor">Buku</li>
                    <li onclick="window.location.href='film.php'" class="pointer-cursor">Film</li>
                </ul>
            </li>
            <li onclick="munculList2('hidden-list2')"><span class="pointer-cursor">Berdasarkan Prodi</span>
                <ul class="hide" id="hidden-list2">
                    <li class="pointer-cursor">Teknik Informatika</li>
                    <li class="pointer-cursor">Sistem Informasi</li>
                    <li class="pointer-cursor">Teknik Arsitektur</li>
                    <li class="pointer-cursor">Desain Produk</li>
                    <li class="pointer-cursor">Akutansi</li>
                    <li class="pointer-cursor">Manajemen</li>
                    <li class="pointer-cursor">Kedokteran</li>
                </ul>
            </li>
            <hr>
            <li><span onclick="window.location.href='aboutUs.php'" class="pointer-cursor">About Us</span></li>
            
        </ul>
    </div>
	
	<div class="single-container">
		<div class="single-content">
            <h1>Buku</h1>
            <table class="showcase-table">
                <tr>
                    <td><img src="img/no-image.jpg" /></td>
                    <td><img src="img/no-image.jpg" /></td> 
                    <td><img src="img/no-image.jpg" /></td>
                    <td><img src="img/no-image.jpg" /></td>
                </tr>
                <tr>
                    <td>Karya A</td>
                    <td>Karya B</td> 
                    <td>Karya C</td>
                    <td>Karya D</td>
                </tr>
            </table>
            
            <hr>
            <table class="showcase-table">
                <tr>
                    <td><img src="img/no-image.jpg" /></td>
                    <td><img src="img/no-image.jpg" /></td> 
                    <td><img src="img/no-image.jpg" /></td>
                    <td><img src="img/no-image.jpg" /></td>
                </tr>
                <tr>
                    <td>Karya A</td>
                    <td>Karya B</td> 
                    <td>Karya C</td>
                    <td>Karya D</td>
                </tr>
            </table>
            
        </div>
	</div>
	
	<footer>
		<div> Dibuat oleh kelompok Show Cabinet UKDW 2014 </div>
	</footer>
	
	
</body>
</html>