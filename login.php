<?php

    session_start();

    require "database.php";
    $koneksi = connect_db();

    $nim = mysqli_real_escape_string($koneksi, $_POST["nim"]);
    $password = mysqli_real_escape_string($koneksi, $_POST["password"]);
    $user = get_user($nim);

    if(empty($_POST["nim"]) || empty($_POST["password"])){
        header("location:index.php?id=1");
    }
    else{
        
        if($password == $user["password"]){
        $_SESSION["NIM"] = $nim;
        $_SESSION["first_name"] = $user["first_name"];
        $_SESSION["last_name"] = $user["last_name"];
        $_SESSION["email"] = $user["email"];
        $_SESSION["gender"] = $user["gender"];
        $_SESSION["secret_word"] = $user["secret_word"];
        $_SESSION["birthday"] = $user["birthday"];
        $_SESSION["prof_pict"] = $user["prof_pict"];

        session_regenerate_id(true);
            if($nim == 'admin')
            {
                header("Location: admin-page.php");
                
            }
            else{
                
                header("Location: index.php");
            }
        }
        else{
            header("location:index.php?id=3");
        }
    }
    

?>