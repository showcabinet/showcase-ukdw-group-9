<?php 
    session_start();

?> 

<!DOCTYPE html>
<html>
<head>
	<title>Showcase | About Us</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="script/script.js"></script>
	
</head>
<body>
	
	<header id="back-header">
        <nav>
            <img src="img/menu-hamburger.svg" id="menu-hamburger" onclick="muncul('hidden-menu')" />
            <img src="img/logo1.png" id="menu-home" onclick="window.location.href='index.php'" />
            <img src="img/logo2.png" class="logo2 animated-logo2"id="menu-home">
            <div class="dropdown-click" id="login">
  			   <?php 
                if(isset($_SESSION["NIM"])){
                    echo "<div class='username-top'><a href='profil.php'>".'<img class="icon-prof-pict" src="'.$_SESSION['prof_pict'].'"/> '.$_SESSION["first_name"]."</a>";
                    echo ', <a href="logout.php">log out</a> </div>';
                }
                else
                {
                    echo
                    '
                    <div class="dropdown-click" id="login">
  			   <button onclick="myFunction()" class="login-btn" style="width: 90px" >Log in</button>
  			   <div id="Login-Click" class="dropdown-content">
                    <form action="login.php" method="post">
                        <div class="username-login">nim: <input type="text" name="nim"> </div>
    		            <div class="password-login">password: <input type="password" name="password"> </div>
    		            <div style="float:right; margin-bottom: 2px; margin-top:5px "> <button type="submit" value="submit">Log in</button> </div>
                        <div onclick="window.location.href="forgotPassword.php"" class="forgot-password">Forgot Password</div>
                    </form>
  			   </div>
		    </div>
            
            <div class="dropdown-click" id="signup">
  			   <button class="login-btn" style="width: 90px" onclick="window.location.href="signUp.php"">Sign Up</button>
  		    </div>
                    ';
                }
            ?>
  		    </div>
        </nav>
        
        
        <header id="bottom-header">
            <ul>
                <li class="nav-border pointer-cursor" onclick="window.location.href='index.php'"><span>Home</span></li>
                <li class="nav-border pointer-cursor" onclick="window.location.href='cek-masuk-profil.php'"><span>Profile</span></li>
                <li class="nav-border pointer-cursor" onclick="window.location.href='semuaKarya.php'"><span>Gallery</span></li>
            </ul>
                
        </header>
    </header>
	
	<div id="hidden-menu" class="hide">
		<h3>Daftar Menu</h3>
        <ul style="list-style:none">
            <li onclick="window.location.href='index.php'" class="pointer-cursor">Home Page</li>
            <li onclick="window.location.href='cek-masuk-profil.php'" class="pointer-cursor">Profile Page</li>
            <hr>
            <li onclick="window.location.href='semuaKarya.php'" class="pointer-cursor">Gallery</li>
            <li onclick="window.location.href='karyaFavorit.php'" class="pointer-cursor">Favorites</li>
            <li onclick="window.location.href='karyaTerbaru.php'" class="pointer-cursor">New</li>
            <hr>
            <li><span onclick="window.location.href='aboutUs.php'" class="pointer-cursor">About Us</span></li>
            
        </ul>
    </div>
	
	<div id="aboutUsContent">
		<div id="aboutUs">
            <h1 style="text-align: center">About Us Show Cabinet UKDW 2014</h1>
            <div>
                <img class="profpict" src="img/juan.jpg">
            </div>
            <div class="descriptionKarya">
                <p><strong>Juan Christian Tjandra</strong><br>
                    Leader of Show Cabinet
                </p>
            </div>
            <div>
                <img class="profpict" src="img/wahyu.jpg">
            </div>
            <div class="descriptionKarya">
                <p><strong>Wahyu Kristanto</strong><br>
                    Member of Show Cabinet
                </p>
            </div>
            <div>
                <img class="profpict" src="img/budi.jpg">
            </div>
            <div class="descriptionKarya">
                <p><strong>Budi Surya Halim</strong><br>
                    Member of Show Cabinet
                </p>
            </div>
            <div>
                <img class="profpict" src="img/lea.jpg">
            </div>
            <div class="descriptionKarya">
                <p><strong>Lea Destiany Taniwel</strong><br>
                    Member of Show Cabinet
                </p>
            </div>
        </div>
	</div>
	
	<footer>
		<div> Dibuat oleh kelompok Show Cabinet UKDW 2014 </div>
	</footer>
	
	
</body>
</html>