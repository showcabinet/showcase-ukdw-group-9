<?php 
    session_start();
?>
<!DOCTYPE html>

<html>
    
<head>
	<title>Showcase | Home</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="script/script.js"></script>
	
</head>
<body>
	
    <header id="back-header">
        <nav>
            <img src="img/menu-hamburger.svg" id="menu-hamburger" onclick="muncul('hidden-menu')" />
            <div onclick="window.location.href='index.php'">
                <img src="img/logo1.png" id="menu-home"  />
                <img src="img/logo2.png" class="logo2 animated-logo2"id="menu-home" onclick="window.location.href='index.php'"/>
            </div>
            
            <div id="hidden-menu" class="hide">
                <h3>Daftar Menu</h3>
                <ul style="list-style:none">
                    <li onclick="window.location.href='index.php'" class="pointer-cursor">Home Page</li>
                    <li onclick="window.location.href='profil.php'" class="pointer-cursor">Profile Page</li>
                    <hr>
                    <li onclick="window.location.href='semuaKarya.php'" class="pointer-cursor">Galery</li>
                    <li onclick="window.location.href='karyaFavorit.php'" class="pointer-cursor">Favorites</li>
                    <li onclick="window.location.href='karyaTerbaru.php'" class="pointer-cursor">New</li>
                    <hr>

                    <li><span onclick="window.location.href='aboutUs.php'" class="pointer-cursor">About Us</span></li>

                </ul>
            </div>
            
            
            
            <div class="username-top"><a href="profil.php"> <?php echo $_SESSION["first_name"]; ?></a></div>
            <form id="search" action="">
                <input type="search" name="search" placeholder="search keyword..." size="30"> 
                <button type="submit">Search</button>
            </form>
        </nav>
        
        
        <header id="bottom-header">
            <ul>
                <li class="nav-border pointer-cursor" onclick="window.location.href='index.php'"><span>Home</span></li>
                <li class="nav-border pointer-cursor" onclick="window.location.href='profil.php'"><span>Profile</span></li>
                <li class="nav-border pointer-cursor" onclick="window.location.href='semuaKarya.php'"><span>Galery</span></li>
            </ul>
                
        </header>
    </header>
    
    
	
    
    <div class="content">
        <div class="container">
            <div id="left-content">
                <h1 onclick="window.location.href='karyaTerbaru.php'" class="pointer-cursor"> Karya Terbaru </h1>
                <div class="containerKarya">
                    <div class="gambarKarya">
                        <img src="img/Android%20Studio.jpg"/>  
                    </div>
                    <div class="descriptionKarya">
                        If we’re going to be entirely honest with one another, I was kind of dreading Deadpool. I love superhero movies and I have often enjoye...
                    </div>
                    <div class="descriptionKarya" style="text-align:right; height:auto; margin-top:10px;"><a href="content.php">Read more...</a>
                    </div>
                </div>
            </div>
            
            <div id="right-content">
                <h1 onclick="window.location.href='karyaFavorit.php'" class="pointer-cursor"> Favorites </h1>
                <div class="containerKarya">
                    <div class="gambarKarya">
                        <img src="img/Android%20Studio.jpg"/>  
                    </div>
                    <div class="descriptionKarya">
                    If we’re going to be entirely honest with one another, I was kind of dreading Deadpool. I love superhero movies and I have often enjoye...
                    </div>
                    <div class="descriptionKarya" style="text-align:right; height:auto; margin-top:10px;"><a href="content.php">Read more...</a></div>
                </div>
                <div class="containerKarya">
                    <div class="gambarKarya">
                        <img src="img/Nike-Jr-Mercurial-Vapor-X-Kids-Firm-Ground-Football-Boot-651620_803_A_PREM.jpg"/>  
                    </div>
                    <div class="descriptionKarya">
                        The Nike Jr. Mercurial Vapor X (4y-6y) Kids' Firm-Ground Soccer Cleat delivers high-speed control and lightweight explosiven...
                    </div>
                    <div class="descriptionKarya" style="text-align:right; height:auto; margin-top:10px;"><a href="content1.php">Read more...</a></div>
                </div>
                <div class="containerKarya">
                    <div class="gambarKarya">
                        <img src="img/Android%20Studio.jpg"/>  
                    </div>
                    <div class="descriptionKarya">
                    If we’re going to be entirely honest with one another, I was kind of dreading Deadpool. I love superhero movies and I have often enjoye...
                    </div>
                    <div class="descriptionKarya" style="text-align:right; height:auto; margin-top:10px;"><a href="content.php">Read more...</a></div>
                </div>
            </div>
        </div>
        
    </div>
	
	
	<footer>
		<div> Dibuat oleh kelompok Show Cabinet UKDW 2014 </div>
	</footer>
	
	
</body>
</html>