<?php
    session_start();
    unset($_SESSION["NIM"]);
    unset($_SESSION["first_name"]);
    unset($_SESSION["last_name"]);
    unset($_SESSION["email"]);
    unset($_SESSION["gender"]);
    unset($_SESSION["secret_word"]);
    unset($_SESSION["birthday"]);
    header("Location: index.php");
?>