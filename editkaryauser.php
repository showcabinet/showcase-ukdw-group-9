<?php 
    session_start();
    if($_SESSION['NIM'] != 'admin'){
        header("Location: index.php?id=2");
    }
    $id = $_GET["id"];
    require "database.php";
    $user = get_user($id);
?> 

<?php
    if(!isset($_SESSION["NIM"])){
        header("Location:loginpage.php");
    }
?>

<!DOCTYPE html>
<html>
<head>
	<title>Showcase | Edit</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="script/script.js"></script>
	
</head>
<body>
	
	<header id="back-header">
        <nav>
            
            
            <?php 
                if(isset($_SESSION["NIM"])){
                    echo '<div class="username-top"><a href="admin-page.php">';
                    echo $_SESSION["first_name"]; 
                    echo '</a> , <a href="logout.php">log out</a> </div>';
                }
                else
                {
                    echo
                    '
                    <div class="dropdown-click" id="login">
  			   <button onclick="myFunction()" class="login-btn" style="width: 90px" >Log in</button>
  			   <div id="Login-Click" class="dropdown-content">
                    <form action="login.php" method="post">
                        <div class="username-login">nim: <input type="text" name="nim"> </div>
    		            <div class="password-login">password: <input type="password" name="password"> </div>
    		            <div style="float:right; margin-bottom: 2px; margin-top:5px "> <button type="submit" value="submit">Log in</button> </div>
                        <div onclick="window.location.href="forgotPassword.php"" class="forgot-password">Forgot Password</div>
                    </form>
  			   </div>
		    </div>
            
            <div class="dropdown-click" id="signup">
  			   <button class="login-btn" style="width: 90px" onclick="window.location.href="signUp.php"">Sign Up</button>
  		    </div>
                    ';
                }
            ?>
            <form id="search" action="">
                <input type="search" name="search" placeholder="search keyword..." size="30"> 
                <button type="submit">Search</button>
            </form>
        </nav>
        
        
        <header id="bottom-header">
            <h3>KARYA <?php echo $user['first_name'];?></h3>
        </header>
    </header>
	
    
    
	
	
    <div class="content">
        <div class="container">
            <?php
                $hasilKarya = array();
                $hasilKarya = get_quote($user['nim']);
                if(count($hasilKarya) >0 ):
                    foreach($hasilKarya as $row):
                ?>
             
            <?php
                
                
                    echo 
                        '<div class="containerKarya">
                    <div class="gambarKarya">
                        <img src="'.$row['gambar_karya'].'"/>  
                    </div>
                    
                    <div class="descriptionKarya">'.$row['judul_karya'].
                    '</div>
                    
                    <div class="descriptionKarya" style="text-align:right; height:auto; margin-top:10px;"><a href="content-admin.php?id='.$row['id_karya'].'">Read more...</a>
                    </div>
                    
                    <div class="descriptionKarya" style="text-align:right; height:auto; margin-top:10px;"><a href="deletekarya.php?id='.$row['id_karya'].'">Delete</a>
                    </div>
                    
                </div>';
                    
                    
                
            ?>
            
            <?php endforeach;
                else: ?>
                <h3>No post</h3>
            <?php endif; ?>
        </div>
        
    </div>
    
	
	
	<footer>
		<div> Dibuat oleh kelompok Show Cabinet UKDW 2014 </div>
	</footer>
	
	
</body>
</html>