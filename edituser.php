<?php 
    session_start();
    if($_SESSION['NIM'] != 'admin'){
        header("Location: index.php?id=2");
    }
    $id = $_GET["id"];
    require "database.php";
    $user = get_user($id);
?> 

<?php
    if(!isset($_SESSION["NIM"])){
        header("Location:loginpage.php");
    }
?>

<!DOCTYPE html>
<html>
<head>
	<title>Showcase | Profile</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="script/script.js"></script>
	
</head>
<body>
	
	<header id="back-header">
        <nav>
            
            
            <?php 
                if(isset($_SESSION["NIM"])){
                    echo '<div class="username-top"><a href="admin-page.php">';
                    echo $_SESSION["first_name"]; 
                    echo '</a> , <a href="logout.php">log out</a> </div>';
                }
                else
                {
                    echo
                    '
                    <div class="dropdown-click" id="login">
  			   <button onclick="myFunction()" class="login-btn" style="width: 90px" >Log in</button>
  			   <div id="Login-Click" class="dropdown-content">
                    <form action="login.php" method="post">
                        <div class="username-login">nim: <input type="text" name="nim"> </div>
    		            <div class="password-login">password: <input type="password" name="password"> </div>
    		            <div style="float:right; margin-bottom: 2px; margin-top:5px "> <button type="submit" value="submit">Log in</button> </div>
                        <div onclick="window.location.href="forgotPassword.php"" class="forgot-password">Forgot Password</div>
                    </form>
  			   </div>
		    </div>
            
            <div class="dropdown-click" id="signup">
  			   <button class="login-btn" style="width: 90px" onclick="window.location.href="signUp.php"">Sign Up</button>
  		    </div>
                    ';
                }
            ?>
            <form id="search" action="">
                <input type="search" name="search" placeholder="search keyword..." size="30"> 
                <button type="submit">Search</button>
            </form>
        </nav>
        
        
        <header id="bottom-header">
            
                
        </header>
    </header>
	
    
    
	
	
    <div class="content">
        <div class="container">
            <form method="POST" action="edit_profile_process.php" enctype="multipart/form-data">
                <label>NIM:</label><br>
				<input type="text" name="nim" id="nim" value="<?php echo $user["nim"]; ?>" readonly><br>
                <label>Full Name:</label><br>
				<input type="text" name="fullname" readonly value="<?php echo $user["first_name"]; ?> <?php echo $user["last_name"]; ?>"><br>
                <label>Email:</label><br>
				<input type="text" name="email" id= "email" value="<?php echo $user['email']; ?>"><br>
                <label>Gender</label><br>
				<input type="text" readonly name="gender" value="<?php echo $user['gender']; ?>" ><br>
				
                <button type="submit">submit</button>
			</form>
        </div>
        
    </div>
    
	
	
	<footer>
		<div> Dibuat oleh kelompok Show Cabinet UKDW 2014 </div>
	</footer>
	
	
</body>
</html>