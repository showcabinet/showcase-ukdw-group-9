<?php 
    session_start();

?> 

<?php
    if(!isset($_SESSION["NIM"])){
        header("Location:loginpage.php");
    }
?>

<!DOCTYPE html>
<html>
<head>
	<title>Showcase | Profile</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="script/script.js"></script>
	
</head>
<body>
	
	<header id="back-header">
        <nav>
            <img src="img/menu-hamburger.svg" id="menu-hamburger" onclick="muncul('hidden-menu')" />
            <div onclick="window.location.href='index.php'">
                <img src="img/logo1.png" id="menu-home"  />
                <img src="img/logo2.png" class="logo2 animated-logo2"id="menu-home" onclick="window.location.href='index.php'"/>
            </div>
            
            <div id="hidden-menu" class="hide">
                <h3>Daftar Menu</h3>
                <ul style="list-style:none">
                    <li onclick="window.location.href='index.php'" class="pointer-cursor">Home Page</li>
                    <li onclick="window.location.href='profil.php'" class="pointer-cursor">Profile Page</li>
                    <hr>
                    <li onclick="window.location.href='semuaKarya.php'" class="pointer-cursor">Galery</li>
                    <li onclick="window.location.href='karyaFavorit.php'" class="pointer-cursor">Favorites</li>
                    <li onclick="window.location.href='karyaTerbaru.php'" class="pointer-cursor">New</li>
                    <hr>

                    <li><span onclick="window.location.href='aboutUs.php'" class="pointer-cursor">About Us</span></li>

                </ul>
            </div>
            
            
            
            
            <?php 
                if(isset($_SESSION["NIM"])){
                    echo "<div class='username-top'><a href='profil.php'>".'<img class="icon-prof-pict" src="'.$_SESSION['prof_pict'].'"/> '.$_SESSION["first_name"]."</a>";
                    echo ', <a href="logout.php">log out</a> </div>';
                }
                else
                {
                    echo
                    '
                    <div class="dropdown-click" id="login">
  			   <button onclick="myFunction()" class="login-btn" style="width: 90px" >Log in</button>
  			   <div id="Login-Click" class="dropdown-content">
                    <form action="login.php" method="post">
                        <div class="username-login">nim: <input type="text" name="nim"> </div>
    		            <div class="password-login">password: <input type="password" name="password"> </div>
    		            <div style="float:right; margin-bottom: 2px; margin-top:5px "> <button type="submit" value="submit">Log in</button> </div>
                        <div onclick="window.location.href="forgotPassword.php"" class="forgot-password">Forgot Password</div>
                    </form>
  			   </div>
		    </div>
            
            <div class="dropdown-click" id="signup">
  			   <button class="login-btn" style="width: 90px" onclick="window.location.href="signUp.php"">Sign Up</button>
  		    </div>
                    ';
                }
            ?>
            <form id="search" action="">
                <input type="search" name="search" placeholder="search keyword..." size="30"> 
                <button type="submit">Search</button>
            </form>
        </nav>
        
        
        <header id="bottom-header">
            <ul>
                <li class="nav-border pointer-cursor" onclick="window.location.href='index.php'"><span>Home</span></li>
                <li class="nav-border pointer-cursor" onclick="window.location.href='profil.php'"><span>Profile</span></li>
                <li class="nav-border pointer-cursor" onclick="window.location.href='semuaKarya.php'"><span>Galery</span></li>
            </ul>
                
        </header>
    </header>
	
    
    
	
	
    <div class="content">
        <div class="container">
            <div id="profile-left-content">
        <h1> Profil </h1>
        <div style="width: max">
            <?php
                if($_SESSION["prof_pict"] == null){
                    echo '<img  class="photo-profile"  src="img/profile-img.jpg" />';
                }
                else
                {
                    echo '<img class="photo-profile" src="data:image/jpeg;base64,'.base64_encode( $_SESSION['prof_pict'] ).'"/>';
                }
            ?>
        </div>
        <input type="file" id="photoUpload">
        <br>
            <button name="upload" value="upload">upload</button>
            <br>
        
        
        <form method="POST" action="edit_profile.php" enctype="multipart/form-data">
                <label>NIM:</label><br>
				<input type="text" name="nim" id="nim" value="<?php echo $_SESSION["NIM"]; ?>" readonly><br>
                <label>Full Name:</label><br>
				<input type="text" name="fullname" id="fullname" readonly value="<?php echo $_SESSION["first_name"]; ?> <?php echo $_SESSION["last_name"]; ?>"><br>
                <label>Email:</label><br>
				<input type="text" name="email" id= "email" value="<?php echo $_SESSION['email']; ?>"><br>
                <label>Gender</label><br>
				<input type="text" readonly name="gender" value="<?php echo $_SESSION['gender']; ?>" ><br>
				
                <button type="submit" value="submit">submit</button>
         </form>
    </div>
	<div id="profile-right-content">
		<h1> <span  onclick="window.location.href='myGalery.php'" class="pointer-cursor">My Galery</span> </h1>
		<div>
            <?php
                require_once("database.php");
                $hasilKarya = array();
                $hasilKarya = get_quote($_SESSION['NIM']);
                if(count($hasilKarya) >0 ):
                    foreach($hasilKarya as $row):
                ?>
             
            <?php
                if(isset($_SESSION['NIM']))
                {
                    echo 
                        '<div class="containerKarya">
                    <div class="gambarKarya">
                        <img src="'.$row['gambar_karya'].'"/>  
                    </div>
                    
                    <div class="descriptionKarya">'.$row['judul_karya'].
                    '</div>
                    
                    <div class="descriptionKarya" style="text-align:right; height:auto; margin-top:10px;"><a href="content.php?id='.$row['id_karya'].'">Read more...</a>
                    </div>
                </div>';
                    }
                    
                    
                
            ?>
            
            <?php endforeach;
                else: ?>
                <h3>No post</h3>
            <?php endif; ?>
            <form method="post" action="upload.php" enctype="multipart/form-data">
                <label>Gambar</label> <br>
                <input type="file" id="gambar" name="gambar"> <br>
                <label>Judul</label> <br>
                <input type="text" name="judul" id="judul"> <br>
                <label>Deskripsi</label> <br>
                <textarea cols="40" rows="10" name = "keterangan"></textarea>
                <input type="submit" name="submit"> <br>
            </form>
            
		</div>
	</div>
        
        </div>
        
    </div>
    
	
	
	<footer>
		<div> Dibuat oleh kelompok Show Cabinet UKDW 2014 </div>
	</footer>
	
	
</body>
</html>