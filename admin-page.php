<?php 
    session_start();
    if($_SESSION['NIM'] != 'admin'){
        header("Location: index.php?id=2");
    }
?> 

<?php
    if(!isset($_SESSION["NIM"])){
        header("Location:loginpage.php");
    }
?>

<!DOCTYPE html>
<html>
<head>
	<title>Showcase | ADMIN</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="script/script.js"></script>
	
</head>
<body>
	
	<header id="back-header">
        <nav>
            <div>
                <img src="img/logo1.png" id="menu-home"  />
                <img src="img/logo2.png" class="logo2 animated-logo2"id="menu-home"/>
            </div>
            
            
            <?php 
                if(isset($_SESSION["NIM"])){
                    echo '<div class="username-top"><a href="admin-page.php">';
                    echo $_SESSION["first_name"]; 
                    echo '</a> , <a href="logout.php">log out</a> </div>';
                }
                else
                {
                    echo
                    '
                    <div class="dropdown-click" id="login">
  			   <button onclick="myFunction()" class="login-btn" style="width: 90px" >Log in</button>
  			   <div id="Login-Click" class="dropdown-content">
                    <form action="login.php" method="post">
                        <div class="username-login">nim: <input type="text" name="nim"> </div>
    		            <div class="password-login">password: <input type="password" name="password"> </div>
    		            <div style="float:right; margin-bottom: 2px; margin-top:5px "> <button type="submit" value="submit">Log in</button> </div>
                        <div onclick="window.location.href="forgotPassword.php"" class="forgot-password">Forgot Password</div>
                    </form>
  			   </div>
		    </div>
            
            <div class="dropdown-click" id="signup">
  			   <button class="login-btn" style="width: 90px" onclick="window.location.href="signUp.php"">Sign Up</button>
  		    </div>
                    ';
                }
            ?>
            <form id="search" action="">
                <input type="search" name="search" placeholder="search keyword..." size="30"> 
                <button type="submit">Search</button>
            </form>
        </nav>
        
        
        <header id="bottom-header">
            
                
        </header>
    </header>
	
    
    
	
	
    <div class="content">
        <div class="container">
            <?php
                require_once "database.php";
                $users = array();
                $users = get_user_data();
                for($i = 0; $i < sizeof($users) ; $i ++)
                {
                    if($users[$i]['nim'] != 'admin')
                    {
                        echo 
                    '<div class="containerKarya">
                        <div class="gambarKarya">
                            <img src="'.$users[$i]['prof_pict'].'"/>
                        </div>
                    
                        <div class="descriptionKarya">Nama: '.$users[$i]['first_name'].' '.$users[$i]['last_name'].
                        '<br>NIM: '.$users[$i]['nim'].'<br>E-mail: '.$users[$i]['email'].'</div> <br>
                        
                    
                        <div class="descriptionKarya" style="text-align:right; height:auto; margin-top:10px;"><a href="edituser.php?id='.$users[$i]['nim'].'">Edit</a>
                        </div>
                        
                        <div class="descriptionKarya" style="text-align:right; height:auto; margin-top:10px;"><a href="editkaryauser.php?id='.$users[$i]['nim'].'">Edit Karya</a>
                        </div>
                        
                        <div class="descriptionKarya" style="text-align:right; height:auto; margin-top:10px;"><a href="deleteuser.php?id='.$users[$i]['nim'].'">Delete</a>
                        
                        </div>
                </div> <br>';
                    }
                
                }
            ?>
            <a class="tambah" href="tambah.php">Tambah akun</a>
        </div>
        
        
    </div>
   
	
	<footer>
		<div> Dibuat oleh kelompok Show Cabinet UKDW 2014 </div>
	</footer>
	
	
</body>
</html>