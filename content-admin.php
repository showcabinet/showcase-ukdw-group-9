<?php 
    session_start();
    if($_SESSION['NIM'] != 'admin'){
        header("Location: index.php?id=2");
    }
    $id = $_GET["id"];
    require "database.php";
    $karya = get_karya_by_id($id);
    $user = get_user($karya["nim"]);
?> 

<!DOCTYPE html>
<html>
<head>
	<title>Showcase | Content</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="script/script.js"></script>
	
</head>
<body>
	
	<header id="back-header">
        <nav>
            <img src="img/menu-hamburger.svg" id="menu-hamburger" onclick="muncul('hidden-menu')" />
            <div>
                <img src="img/logo1.png" id="menu-home"  />
                <img src="img/logo2.png" class="logo2 animated-logo2"id="menu-home" />
            </div>
            
            
            
            
            <?php 
                if(isset($_SESSION["NIM"])){
                    echo "<div class='username-top'><a href='admin-page.php'>".'<img class="icon-prof-pict" src="'.$_SESSION['prof_pict'].'"/> '.$_SESSION["first_name"]."</a>";
                    echo ', <a href="logout.php">log out</a> </div>';
                }
                else
                {
                    echo
                    '
                    <div class="dropdown-click" id="login">
  			   <button onclick="myFunction()" class="login-btn" style="width: 90px" >Log in</button>
  			   <div id="Login-Click" class="dropdown-content">
                    <form action="login.php" method="post">
                        <div class="username-login">nim: <input type="text" name="nim"> </div>
    		            <div class="password-login">password: <input type="password" name="password"> </div>
    		            <div style="float:right; margin-bottom: 2px; margin-top:5px "> <button type="submit" value="submit">Log in</button> </div>
                        <div onclick="window.location.href="forgotPassword.php"" class="forgot-password">Forgot Password</div>
                    </form>
  			   </div>
		    </div>
            
            <div class="dropdown-click" id="signup">
  			   <button class="login-btn" style="width: 90px" onclick="window.location.href="signUp.php"">Sign Up</button>
  		    </div>
                    ';
                }
            ?>
            <form id="search" action="">
                <input type="search" name="search" placeholder="search keyword..." size="30"> 
                <button type="submit">Search</button>
            </form>
        </nav>
        
        
        <header id="bottom-header">
            
                
        </header>
    </header>
	
	
	
	<div class="content">
		<center> 
            <?php echo '<img src="'.$karya['gambar_karya'].'" style="height: 200px;">';
            ?> 
        </center>
        <center><?php echo date($karya['tanggal_upload']).' By: '.$user["first_name"].' '.$user["last_name"]; ?></center>
        <div style="padding:10px">
            
            <?php
                echo $karya['keterangan'].'<br>';
            
            ?>
            <br>
            <form>
                Comment:<br>
                <textarea name="comment" id="comment" style=" height:200px; width:400px ; "></textarea>
                <br>
                <input type='submit' value='Submit' />
            </form>
        </div>
        
        
	</div>
	
	<footer>
		<div> Dibuat oleh kelompok Show Cabinet UKDW 2014 </div>
	</footer>
	
	
</body>
</html>