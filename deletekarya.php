<?php
    session_start();
    if($_SESSION['NIM'] != 'admin'){
        header("Location: index.php?id=2");
    }
    else
    {
        $id = $_GET["id"];
        require "database.php";
        $karya = get_karya_by_id($id);
        delete_karya($id);
        
        header("Location: admin-page.php");
    }
   
?>